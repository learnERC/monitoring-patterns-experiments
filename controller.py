from datetime import datetime
import getopt
import sys
import time

import ansible_runner
from utils import load_pattern_experiments, is_pattern_config_already_executed, set_pattern_config_run_start, set_pattern_config_run_end


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hp:l:", ["pattern=", "logstash_host="])
    except getopt.GetoptError:
        print('controller.py -p <pattern> -l <logstash_host>')
        sys.exit(2)
    
    pattern = None
    logstash_host = None
    
    for opt, arg in opts:
        if opt == '-h':
            print('controller.py -p <pattern> -l <logstash_host>')
            sys.exit()
        elif opt in ("-p", "--pattern"):
            pattern = arg
        elif opt in ("-l", "--logstash_host"):
            logstash_host = arg
    
    if not pattern or not logstash_host:
        print('controller.py -p <pattern> -l <logstash_host>')
        sys.exit(2)
    execute_pattern_experiments(pattern, logstash_host)


def setup(n_targets, n_monitoring_units):
    result = ansible_runner.run(private_data_dir='.', playbook='setup.yml', roles_path='./roles',
                                extravars={'targets': n_targets, 'monitoring_units': n_monitoring_units})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def teardown(n_targets, n_monitoring_units):
    result = ansible_runner.run(private_data_dir='.', playbook='teardown.yml', roles_path='./roles',
                                extravars={'targets': n_targets, 'monitoring_units': n_monitoring_units})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def setup_single_run(run_id, pattern, n_users, n_targets, n_kpis, n_probes, n_monitoring_units, logstash_host):
    result = ansible_runner.run(private_data_dir='.', playbook='setup-single-run.yml', roles_path='./roles',
                                extravars={
                                    'run_id': run_id, 'pattern': pattern, 'users': n_users, 'targets': n_targets,
                                    'kpis': n_kpis, 'probes': n_probes, 'monitoring_units': n_monitoring_units,
                                    'logstash_host': logstash_host})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def teardown_single_run(pattern, n_monitoring_units, n_targets, n_users, n_probes):
    result = ansible_runner.run(private_data_dir='.', playbook='teardown-single-run.yml', roles_path='./roles',
                                extravars={'pattern': pattern, 'monitoring_units': n_monitoring_units, 'targets': n_targets, 'users': n_users, 'probes': n_probes})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def execute_pattern_experiments(pattern, logstash_host):
    pattern_experiments = load_pattern_experiments(pattern)
    for experiment_id, configs in pattern_experiments.items():
        max_targets = configs.get('max_targets')
        max_monitoring_units = configs.get('max_monitoring_units')

        setup(max_targets, max_monitoring_units)
        
        # Sleep 2 minutes to wait for VMs connection...sometimes it takes more time and Ansible pre_tasks fails
        time.sleep(60 * 2)

        for run in configs.get('runs'):
            users = run.get('users')
            targets = run.get('targets')
            kpis = run.get('kpis')
            pattern_config_key = '{}-{}-{}-{}'.format(pattern, users, targets, kpis)
            if not is_pattern_config_already_executed(pattern_config_key):
                probes = kpis
                monitoring_units = run.get('monitoring_units')
                for i in range(1, 4):
                    setup_single_run(i, pattern, users, targets, kpis, probes, monitoring_units, logstash_host)
                    set_pattern_config_run_start(pattern_config_key, i, datetime.now())
                    time.sleep(60 * 10)
                    set_pattern_config_run_end(pattern_config_key, i, datetime.now())
                    teardown_single_run(pattern, monitoring_units, targets, users, probes)
        teardown(max_targets, max_monitoring_units)


if __name__ == "__main__":
    main(sys.argv[1:])
