import os

import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.ticker as mticker
import utils

markers = {
    'RESERVED_SIDECAR_MULTI_PROBE': 'o',
    'SHARED_SIDECAR_MULTI_PROBE': 's',
    'RESERVED_GLOBAL_UNIT_MULTI_PROBE': 'd',
    'SHARED_GLOBAL_UNIT_MULTI_PROBE': '*',
    'FULLY_SHARED_SIDECAR_1': 'h',
    'RESERVED_SIDECAR_1': 'p',
    'RESERVED_UNIT_1': 'x',
    'FULLY_SHARED_UNIT_1': '1',
    'INTERNAL_UNIT_STAR': '>',
    'PARTIALLY_SHARED_UNIT_STAR': '<',
    'PARTIALLY_SHARED_SIDECAR_STAR': '^',
}

xticks_targets_2 = ['02-01-01', '02-02-01', '02-04-01', '02-08-01', '02-16-01']
xticks_users_2 = ['01-01-01', '02-01-02', '04-01-04', '08-01-08', '16-01-16']

def replace_key(key):
    parts = key.split('-')
    pattern_name = parts[0]
    numbers = parts[1:]

    users = numbers[0]
    targets = numbers[1]
    kpis = numbers[2]

    if int(users) < 10:
        users = '0{}'.format(users)

    if int(targets) < 10:
        targets = '0{}'.format(targets)

    if int(kpis) < 10:
        kpis = '0{}'.format(kpis)

    return '{}-{}-{}'.format(users, targets, kpis)


def generate_single_experiment_plot(experiment_id, patterns, resource, metric, patterns_type):
    fig, ax = plt.subplots()
    fig.suptitle('{} {}'.format(experiment_id, utils.NEW_RESOURCE_NAMES[resource]).upper())
    for pattern in patterns:
        if pattern == 'INTERNAL_UNIT_STAR' and resource == 'monitoring-units':
            if metric == 'cpu' or metric == 'memory':
                continue
            else:
                df = pd.read_csv('./dataset/split/{}/{}/targets/{}.csv'.format(pattern, experiment_id, metric),
                                 parse_dates=True)
        elif pattern == 'INTERNAL_UNIT_STAR' and resource == 'targets':
            continue
        else:
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                             parse_dates=True)
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        if pattern == 'INTERNAL_UNIT_STAR' and resource == 'monitoring-units':
            df['resource'] = df['resource'].str.replace(r'target-', 'monitoring-unit-')
            mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
                by=['key', 'run_id']).sum().groupby(
                by=['key']).mean()
            std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
                by=['key', 'run_id']).sum().groupby(
                by=['key']).sem()
            ax.errorbar(df['key'].unique(), mean, yerr=std,
                    label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern])
        elif resource == 'monitoring-units':
            mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
                by=['key', 'run_id']).sum().groupby(
                by=['key']).mean()
            std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
                by=['key', 'run_id']).sum().groupby(
                by=['key']).sem()
            ax.errorbar(df['key'].unique(), mean, yerr=std,
                        label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern])
        elif resource == 'targets':
            mean = df.groupby(by=['key', 'run_id'])['value'].median().groupby(
                by=['key']).mean()
            std = df.groupby(by=['key', 'run_id'])['value'].median().groupby(by=['key']).sem()
            ax.errorbar(df['key'].unique(), mean, yerr=std,
                    label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern])
    ax.tick_params(axis='x', rotation=45)

    if metric == 'cpu':
        if patterns_type == 'container-patterns':
            label = 'CPU (nanocores)'
        else:
            label = 'CPU (%)'
        plt.setp(ax, ylabel=label)
    elif metric == 'memory':
        label = 'Memory (GiB)'
        plt.setp(ax, ylabel=label)
    elif metric == 'network_in':
        label = 'Network In (MiB)'
        plt.setp(ax, ylabel=label)
    elif metric == 'network_out':
        label = 'Network Out (MiB)'
        plt.setp(ax, ylabel=label)

    plt.setp(ax, xlabel='Configuration Triplet (Users-Targets-KPIs)')

    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles, labels, bbox_to_anchor=(1, 0.5), loc="center left", borderaxespad=0)

    fig.tight_layout()

    filename = '{}_{}'.format(metric, resource).upper()
    os.makedirs('./plots/{}/{}'.format(patterns_type, experiment_id), exist_ok=True)
    plt.savefig('./plots/{}/{}/{}.pdf'.format(patterns_type, experiment_id, filename), bbox_inches="tight")
    plt.close()


def generate_paper_plots_grid_containers():
    # containers plots
    fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(1, 5, figsize=(20, 4))
    fig.suptitle('Application-oriented Probe Holders')
    resource = 'monitoring-units'

    # memory - increasing users 2
    metric = 'memory'
    experiment_id = 'INCREASING_USERS_2'
    for pattern in utils.CONTAINER_PATTERNS:
        df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                         parse_dates=True)
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).mean()
        std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).sem()
        ax1.errorbar(df['key'].unique(), mean, yerr=std,
                 label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
        ax1.set_title(experiment_id)
        ax1.tick_params(axis='x', rotation=45)
    plt.setp(ax1, ylabel='Memory (GiB)')
    
    # network_in - increasing targets 2
    metric = 'network_in'
    experiment_id = 'INCREASING_TARGETS_2'
    for pattern in utils.CONTAINER_PATTERNS:
        df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                         parse_dates=True)
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).mean()
        std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).sem()
        ax2.errorbar(df['key'].unique(), mean, yerr=std,
                 label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
        ax2.set_title(experiment_id)
        ax2.tick_params(axis='x', rotation=45)
        ticks_loc = ax2.get_xticks()
        ax2.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        ax2.set_xticklabels([x for x in xticks_targets_2])
    plt.setp(ax2, ylabel='Network In (MiB)')
    
    # network_in - increasing users 2
    metric = 'network_in'
    experiment_id = 'INCREASING_USERS_2'
    for pattern in utils.CONTAINER_PATTERNS:
        df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                         parse_dates=True)
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).mean()
        std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).sem()
        ax3.errorbar(df['key'].unique(), mean, yerr=std,
                 label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
        ax3.set_title(experiment_id)
        ax3.tick_params(axis='x', rotation=45)
        ticks_loc = ax3.get_xticks()
        ax3.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        ax3.set_xticklabels([x for x in xticks_users_2])
    plt.setp(ax3, ylabel='Network In (MiB)', xlabel='Configuration Triplet (Users-Targets-KPIs)')

    # network_out - increasing targets 2
    metric = 'network_out'
    experiment_id = 'INCREASING_TARGETS_2'
    for pattern in utils.CONTAINER_PATTERNS:
        df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                         parse_dates=True)
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).mean()
        std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).sem()
        ax4.errorbar(df['key'].unique(), mean, yerr=std,
                 label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
        ax4.set_title(experiment_id)
        ax4.tick_params(axis='x', rotation=45)
        ticks_loc = ax4.get_xticks()
        ax4.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        ax4.set_xticklabels([x for x in xticks_targets_2])
    plt.setp(ax4, ylabel='Network Out (MiB)')

    # network_out - increasing users 2
    metric = 'network_out'
    experiment_id = 'INCREASING_USERS_2'
    for pattern in utils.CONTAINER_PATTERNS:
        df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                         parse_dates=True)
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).mean()
        std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).sem()
        ax5.errorbar(df['key'].unique(), mean, yerr=std,
                 label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
        ax5.set_title(experiment_id)
        ax5.tick_params(axis='x', rotation=45)
        ticks_loc = ax5.get_xticks()
        ax5.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        ax5.set_xticklabels([x for x in xticks_users_2])
    plt.setp(ax5, ylabel='Network Out (MiB)')

    handles, labels = ax1.get_legend_handles_labels()
    fig.legend(handles, labels, ncol=len(labels), bbox_to_anchor=(0.5, -0.05), loc="lower center", borderaxespad=0)

    fig.tight_layout()

    filename = 'PAPER_GRID'
    plt.savefig('./plots/{}/{}.pdf'.format('container-patterns', filename), bbox_inches="tight")
    plt.close()
    
    
def generate_paper_plots_grid_vms():
    # vm plots
    fig, (ax1, ax2, ax3, ax4, ax5, ax6) = plt.subplots(1, 6, figsize=(20, 4))
    fig.suptitle('System-oriented Probe Holders')
    resource = 'monitoring-units'
    
    # memory - increasing targets 2
    metric = 'memory'
    experiment_id = 'INCREASING_TARGETS_2'
    for pattern in utils.VM_PATTERNS:
        if pattern == 'INTERNAL_UNIT_STAR':
            pass
        else:
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                             parse_dates=True)
            df['key'] = df['key'].map(replace_key)
            df['value'] = df['value'].astype(float)
            mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
                by=['key']).mean()
            std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
                by=['key']).sem()
            ax1.errorbar(df['key'].unique(), mean, yerr=std,
                     label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
            ax1.set_title(experiment_id)
            ax1.tick_params(axis='x', rotation=45)
    plt.setp(ax1, ylabel='Memory (GiB)')

    # memory - increasing users 2
    metric = 'memory'
    experiment_id = 'INCREASING_USERS_2'
    for pattern in utils.VM_PATTERNS:
        if pattern == 'INTERNAL_UNIT_STAR':
            pass
        else:
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                             parse_dates=True)
            df['key'] = df['key'].map(replace_key)
            df['value'] = df['value'].astype(float)
            mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
                by=['key']).mean()
            std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
                by=['key']).sem()
            ax2.errorbar(df['key'].unique(), mean, yerr=std,
                     label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
            ax2.set_title(experiment_id)
            ax2.tick_params(axis='x', rotation=45)
    plt.setp(ax2, ylabel='Memory (GiB)', xlabel='Configuration Triplet (Users-Targets-KPIs)')
    
    # network_in - increasing targets 2
    metric = 'network_in'
    experiment_id = 'INCREASING_TARGETS_2'
    for pattern in utils.VM_PATTERNS:
        if pattern == 'INTERNAL_UNIT_STAR':
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, 'targets', metric),
                             parse_dates=True)
        else:
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                             parse_dates=True)
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        if pattern == 'INTERNAL_UNIT_STAR':
            df['resource'] = df['resource'].str.replace(r'target-', 'monitoring-unit-')
        mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
            by=['key', 'run_id']).sum().groupby(
            by=['key']).mean()
        std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
            by=['key', 'run_id']).sum().groupby(
            by=['key']).sem()
        ax3.errorbar(df['key'].unique(), mean, yerr=std,
                 label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
        ax3.set_title(experiment_id)
        ax3.tick_params(axis='x', rotation=45)
        ticks_loc = ax3.get_xticks()
        ax3.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        ax3.set_xticklabels([x for x in xticks_targets_2])
    plt.setp(ax3, ylabel='Network In (MiB)')

    # network_in - increasing users 2
    metric = 'network_in'
    experiment_id = 'INCREASING_USERS_2'
    for pattern in utils.VM_PATTERNS:
        if pattern == 'INTERNAL_UNIT_STAR':
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, 'targets', metric),
                             parse_dates=True)
        else:
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                             parse_dates=True)
        if pattern == 'INTERNAL_UNIT_STAR':
            df['resource'] = df['resource'].str.replace(r'target-', 'monitoring-unit-')
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).mean()
        std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).sem()
        ax4.errorbar(df['key'].unique(), mean, yerr=std,
                 label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
        ax4.set_title(experiment_id)
        ax4.tick_params(axis='x', rotation=45)
        ticks_loc = ax4.get_xticks()
        ax4.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        ax4.set_xticklabels([x for x in xticks_users_2])
    plt.setp(ax4, ylabel='Network In (MiB)')

    # network_out - increasing targets 2
    metric = 'network_out'
    experiment_id = 'INCREASING_TARGETS_2'
    for pattern in utils.VM_PATTERNS:
        if pattern == 'INTERNAL_UNIT_STAR':
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, 'targets', metric),
                             parse_dates=True)
        else:
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                             parse_dates=True)
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        if pattern == 'INTERNAL_UNIT_STAR':
            df['resource'] = df['resource'].str.replace(r'target-', 'monitoring-unit-')
        mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).mean()
        std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).sem()
        ax5.errorbar(df['key'].unique(), mean, yerr=std,
                 label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
        ax5.set_title(experiment_id)
        ax5.tick_params(axis='x', rotation=45)
        ticks_loc = ax5.get_xticks()
        ax5.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        ax5.set_xticklabels([x for x in xticks_targets_2])
    plt.setp(ax5, ylabel='Network Out (MiB)', xlabel='Configuration Triplet (Users-Targets-KPIs)')

    # network_out - increasing users 2
    metric = 'network_out'
    experiment_id = 'INCREASING_USERS_2'
    for pattern in utils.VM_PATTERNS:
        if pattern == 'INTERNAL_UNIT_STAR':
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, 'targets', metric),
                             parse_dates=True)
        else:
            df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                             parse_dates=True)
        df['key'] = df['key'].map(replace_key)
        df['value'] = df['value'].astype(float)
        if pattern == 'INTERNAL_UNIT_STAR':
            df['resource'] = df['resource'].str.replace(r'target-', 'monitoring-unit-')
        mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).mean()
        std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(by=['key', 'run_id']).sum().groupby(
            by=['key']).sem()
        ax6.errorbar(df['key'].unique(), mean, yerr=std,
                 label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern], capsize=4)
        ax6.set_title(experiment_id)
        ax6.tick_params(axis='x', rotation=45)
        ticks_loc = ax6.get_xticks()
        ax6.xaxis.set_major_locator(mticker.FixedLocator(ticks_loc))
        ax6.set_xticklabels([x for x in xticks_users_2])
    plt.setp(ax6, ylabel='Network Out (MiB)')

    handles, labels = ax5.get_legend_handles_labels()
    fig.legend(handles, labels, ncol=len(labels), bbox_to_anchor=(0.5, -0.1), loc="lower center", borderaxespad=0)

    fig.tight_layout()

    filename = 'PAPER_GRID'
    plt.savefig('./plots/{}/{}.pdf'.format('vm-patterns', filename), bbox_inches="tight")
    plt.close()


def generate_plots(patterns, resource, metric, patterns_type):
    fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, figsize=(10, 10), sharey=True)
    fig.suptitle('{} {}'.format(metric, utils.NEW_RESOURCE_NAMES[resource]).upper())

    axes = [ax1, ax2, ax3, ax4, ax5, ax6]
    i = 0

    for experiment_id in utils.EXPERIMENT_IDS:
        for pattern in patterns:
            if pattern == 'INTERNAL_UNIT_STAR' and resource == 'monitoring-units':
                if metric == 'cpu' or metric == 'memory':
                    continue
                else:
                    df = pd.read_csv('./dataset/split/{}/{}/targets/{}.csv'.format(pattern, experiment_id, metric),
                                     parse_dates=True)
            elif pattern == 'INTERNAL_UNIT_STAR' and resource == 'targets':
                continue
            else:
                df = pd.read_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, resource, metric),
                                 parse_dates=True)
            df['key'] = df['key'].map(replace_key)
            df['value'] = df['value'].astype(float)
            if pattern == 'INTERNAL_UNIT_STAR' and resource == 'monitoring-units':
                df['resource'] = df['resource'].str.replace(r'target-', 'monitoring-unit-')
                mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
                    by=['key', 'run_id']).sum().groupby(
                    by=['key']).mean()
                std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
                    by=['key', 'run_id']).sum().groupby(
                    by=['key']).sem()
                axes[i].errorbar(df['key'].unique(), mean, yerr=std, label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern])
            elif resource == 'monitoring-units':
                mean = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
                    by=['key', 'run_id']).sum().groupby(
                    by=['key']).mean()
                std = df.groupby(by=['key', 'resource', 'run_id'])['value'].median().groupby(
                    by=['key', 'run_id']).sum().groupby(
                    by=['key']).sem()
                axes[i].errorbar(df['key'].unique(), mean, yerr=std, label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern])
            elif resource == 'targets':
                mean = df.groupby(by=['key', 'run_id'])['value'].median().groupby(by=['key']).mean()
                std = df.groupby(by=['key', 'run_id'])['value'].median().groupby(by=['key']).sem()
                axes[i].errorbar(df['key'].unique(), mean, yerr=std, label=utils.NEW_PATTERN_NAMES[pattern], marker=markers[pattern])
        axes[i].set_title(experiment_id)
        axes[i].tick_params(axis='x', rotation=45)
        i = i + 1

    if metric == 'cpu':
        if patterns_type == 'container-patterns':
            label = 'CPU (nanocores)'
        else:
            label = 'CPU (%)'
        plt.setp(ax1, ylabel=label)
        plt.setp(ax3, ylabel=label)
        plt.setp(ax5, ylabel=label)
    elif metric == 'memory':
        label = 'Memory (GiB)'
        plt.setp(ax1, ylabel=label)
        plt.setp(ax3, ylabel=label)
        plt.setp(ax5, ylabel=label)
    elif metric == 'network_in':
        label = 'Network In (MiB)'
        plt.setp(ax1, ylabel=label)
        plt.setp(ax3, ylabel=label)
        plt.setp(ax5, ylabel=label)
    elif metric == 'network_out':
        label = 'Network Out (MiB)'
        plt.setp(ax1, ylabel=label)
        plt.setp(ax3, ylabel=label)
        plt.setp(ax5, ylabel=label)

    plt.setp(ax5, xlabel='Configuration Triplet (Users-Targets-KPIs)')
    plt.setp(ax6, xlabel='Configuration Triplet (Users-Targets-KPIs)')

    handles, labels = axes[0].get_legend_handles_labels()
    fig.legend(handles, labels, bbox_to_anchor=(1, 0), loc="lower left", borderaxespad=0)

    fig.tight_layout()

    filename = '{}_{}'.format(metric, resource).upper()
    plt.savefig('./plots/{}/{}.pdf'.format(patterns_type, filename), bbox_inches="tight")
    plt.close()


if __name__ == '__main__':
    generate_paper_plots_grid_containers()
    generate_paper_plots_grid_vms()
    for m in utils.METRICS:
        generate_plots(utils.VM_PATTERNS, 'monitoring-units', m, 'vm-patterns')
        generate_plots(utils.VM_PATTERNS, 'targets', m, 'vm-patterns')

        generate_plots(utils.CONTAINER_PATTERNS, 'monitoring-units', m, 'container-patterns')
        generate_plots(utils.CONTAINER_PATTERNS, 'targets', m, 'container-patterns')

    for experiment_id in utils.EXPERIMENT_IDS:
        for m in utils.METRICS:
            generate_single_experiment_plot(experiment_id, utils.VM_PATTERNS, 'monitoring-units', m, 'vm-patterns')
            generate_single_experiment_plot(experiment_id, utils.VM_PATTERNS, 'targets', m, 'vm-patterns')

            generate_single_experiment_plot(experiment_id, utils.CONTAINER_PATTERNS, 'monitoring-units', m,
                                            'container-patterns')
            generate_single_experiment_plot(experiment_id, utils.CONTAINER_PATTERNS, 'targets', m, 'container-patterns')
