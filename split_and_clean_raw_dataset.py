import sys

import pandas as pd
import json
import os
import logging
from datetime import datetime
import utils

logging.basicConfig(stream=sys.stdout, level=logging.ERROR)


def load_pattern_experiments_configs(pattern):
    with open(utils.EXPERIMENTS_PLAN_FILE, 'r') as f:
        experiments = json.load(f)
    return experiments.get(pattern)


def load_pattern_experiments_runs(pattern):
    with open(utils.PATTERN_RUNS_FILE, 'r') as f:
        runs = json.load(f)
    pattern_runs = {}
    for key, item in runs.items():
        if key.startswith(pattern):
            pattern_runs[key] = item
    return pattern_runs


def get_runs_date_intervals(key, experiments_runs):
    date_intervals = []
    runs = experiments_runs.get(key)
    for run_id, interval in runs.items():
        start_time = datetime.utcfromtimestamp(interval['start_time']).strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        end_time = datetime.utcfromtimestamp(interval['end_time']).strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        date_intervals.append((start_time, end_time))
    return date_intervals


def split_raw_pattern_dataset(pattern, experiments_configs, experiments_runs):
    logging.info('Splitting raw datatest for pattern %s', pattern)

    for experiment_id, configs in experiments_configs.items():
        logging.info('Experiment %s', experiment_id)

        for metric in utils.METRICS:
            logging.info('Metric %s', metric)

            df = pd.DataFrame(columns=['timestamp', 'key', 'run_id', 'resource', 'metric', 'value'])
            raw_df = pd.read_csv('./dataset/raw/{}-{}.csv'.format(pattern, metric),
                                 names=['timestamp', 'key', 'run_id', 'resource', 'metric', 'value'], parse_dates=True)
            raw_df_groups = raw_df.groupby(by='key')

            for run in configs.get('runs'):
                users = run.get('users')
                targets = run.get('targets')
                kpis = run.get('kpis')
                key = '{}-{}-{}-{}'.format(pattern, users, targets, kpis)

                raw_df_group = raw_df_groups.get_group(key)
                logging.info('Extracting valid data for key %s', key)
                df = extract_valid_experiments_time_windows(df, experiments_runs, key, raw_df_group)

            targets_df = df.groupby(df['resource'].str.extract('(^target)')[0]).get_group('target')

            if metric == 'network_in' or metric == 'network_out':
                logging.info('Resampling cumulative network data')
                targets_df = resample_cumulative_network_data(targets_df, metric)
            if metric == 'memory':
                targets_df['value'] = targets_df['value'] / 1024 / 1024 / 1024  # Bytes to GiB

            os.makedirs('./dataset/split/{}/{}/{}'.format(pattern, experiment_id, 'targets'), exist_ok=True)
            targets_df.to_csv('./dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, 'targets', metric),
                              index=False)

            if pattern != 'INTERNAL_UNIT_STAR':
                monitoring_units_df = df.groupby(df['resource'].str.extract('(^monitoring-unit)')[0]).get_group(
                    'monitoring-unit')

                if metric == 'network_in' or metric == 'network_out':
                    monitoring_units_df = resample_cumulative_network_data(monitoring_units_df, metric)

                if metric == 'memory':
                    monitoring_units_df['value'] = monitoring_units_df['value'] / 1024 / 1024 / 1024  # Bytes to GiB

                os.makedirs('./dataset/split/{}/{}/{}'.format(pattern, experiment_id, 'monitoring-units'),
                            exist_ok=True)
                monitoring_units_df.to_csv(
                    './dataset/split/{}/{}/{}/{}.csv'.format(pattern, experiment_id, 'monitoring-units', metric),
                    index=False)


def extract_valid_experiments_time_windows(df, experiments_runs, key, raw_df_group):
    date_intervals = get_runs_date_intervals(key, experiments_runs)
    for i in date_intervals:
        df = pd.concat([df, raw_df_group[
            (raw_df_group['timestamp'] >= i[0]) & (raw_df_group['timestamp'] <= i[1])]])
    return df


def resample_cumulative_network_data(original_df, metric):
    new_df = pd.DataFrame(columns=['timestamp', 'key', 'run_id', 'resource', 'metric', 'value'])
    groups = original_df.groupby(by=['key', 'run_id', 'resource'])
    for group_id, group_df in groups:
        group_df.sort_values(by=['timestamp'], inplace=True)
        value_end = group_df.tail(1)['value'].values[0]
        value_start = group_df.head(1)['value'].values[0]
        if value_end < value_start:
            logging.error('Value end is greater than value start key %s run %s resource %s', group_id[0], group_id[1],
                          group_id[2])
        else:
            new_df = new_df.append({
                'timestamp': group_df.tail(1)['timestamp'].values[0],
                'key': group_id[0],
                'run_id': group_id[1],
                'resource': group_id[2],
                'metric': metric,
                'value': (value_end - value_start) / 1024 / 1024  # Bytes to MiB
            }, ignore_index=True)
    return new_df


if __name__ == '__main__':
    for p in utils.VM_PATTERNS:
        pattern_experiments_configs = load_pattern_experiments_configs(p)
        pattern_experiments_runs = load_pattern_experiments_runs(p)
        split_raw_pattern_dataset(p, pattern_experiments_configs, pattern_experiments_runs)

    for p in utils.CONTAINER_PATTERNS:
        pattern_experiments_configs = load_pattern_experiments_configs(p)
        pattern_experiments_runs = load_pattern_experiments_runs(p)
        split_raw_pattern_dataset(p, pattern_experiments_configs, pattern_experiments_runs)
