import getopt
import sys

import ansible_runner


def main(argv):
    try:
        opts, args = getopt.getopt(argv, "ht:m:", ["targets=", "monitoring_units="])
    except getopt.GetoptError:
        print('create_resources.py -t <targets> -m <monitoring_units>')
        sys.exit(2)

    targets = None
    monitoring_units = None

    for opt, arg in opts:
        if opt == '-h':
            print('create_resources.py -t <targets> -m <monitoring_units>')
            sys.exit()
        elif opt in ("-t", "--targets"):
            targets = arg
        elif opt in ("-m", "--monitoring_units"):
            monitoring_units = arg

    if not targets or not monitoring_units:
        print('create_resources.py -t <targets> -m <monitoring_units>')
        sys.exit(2)
    create_targets(targets)
    create_monitoring_units(monitoring_units)


def create_targets(n_targets):
    result = ansible_runner.run(private_data_dir='.', playbook='create-targets.yml', roles_path='./roles',
                                extravars={'targets': n_targets})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def create_monitoring_units(n_monitoring_units):
    result = ansible_runner.run(private_data_dir='.', playbook='create-monitoring-units.yml', roles_path='./roles',
                                extravars={'monitoring_units': n_monitoring_units})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


if __name__ == "__main__":
    main(sys.argv[1:])
