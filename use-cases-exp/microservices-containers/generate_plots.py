import logging
import os

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

plt.rcParams['font.size'] = 14

METRICS = ['cpu', 'memory', 'network_in', 'network_out']
DATA_DIR = "data"
SCALE = [1, 2, 4, 8, 16]
RUNS = [1, 2, 3]


def _resample_cumulative_network_data(original_df, metric):
    new_df = pd.DataFrame(columns=['timestamp', 'replicas', 'run', 'resource', 'metric', 'value'])
    groups = original_df.groupby(by=['replicas', 'run', 'resource'])
    for group_id, group_df in groups:
        group_df.sort_values(by=['timestamp'], inplace=True)
        value_end = group_df.tail(1)['value'].values[0]
        value_start = group_df.head(1)['value'].values[0]
        if value_end < value_start:
            logging.error('Value end is greater than value start key %s run %s resource %s', group_id[0], group_id[1],
                          group_id[2])
        else:
            new_df = new_df.append({
                'timestamp': group_df.tail(1)['timestamp'].values[0],
                'replicas': group_id[0],
                'run': group_id[1],
                'resource': group_id[2],
                'metric': metric,
                'value': (value_end - value_start) / 1024 / 1024  # Bytes to MiB
            }, ignore_index=True)
    new_df['value'].astype(float)
    return new_df


def _generate_shared_probe_holder_plots():
    # Memory
    df = pd.DataFrame(columns=['replicas', 'run', 'resource', 'value'])
    for s in SCALE:
        for r in RUNS:
            csv_filepath = f"{DATA_DIR}/cartservice/{s}/{r}/memory-probe-holders.csv"
            run_df = pd.read_csv(csv_filepath, names=['timestamp', 'resource', 'metric', 'value'], header=None,
                                 parse_dates=True)
            run_df = run_df.drop(columns=['timestamp', 'metric'])
            run_df = run_df[run_df['resource'] == 'blackbox-exporter']
            run_df['value'] = run_df['value'] / 1024 / 1024 / 1024  # Bytes to GiB
            run_df['value'] = run_df['value'].astype(float)
            run_df['replicas'] = s
            run_df['run'] = r
            df = pd.concat([df, run_df])
    mean = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).mean()
    std = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).sem()
    
    print("Shared-T*P1 Memory")
    print(mean)

    plt.errorbar(df['replicas'].unique(), mean, yerr=std, capsize=4)
    plt.title('Shared-T*P1 Probe Holder Memory Consumption', pad=20)
    plt.xlabel('Cartservice Replicas')
    plt.ylabel('Memory (GiB)')
    plt.yticks(np.arange(0, 0.3, step=0.1))
    plt.xticks(SCALE)
    plt.tight_layout()
    plt.savefig('plots/memory_shared-t*p1.pdf')
    plt.clf()

    # CPU
    df = pd.DataFrame(columns=['replicas', 'run', 'resource', 'value'])
    for s in SCALE:
        for r in RUNS:
            csv_filepath = f"{DATA_DIR}/cartservice/{s}/{r}/cpu-probe-holders.csv"
            run_df = pd.read_csv(csv_filepath, names=['timestamp', 'resource', 'metric', 'value'], header=None,
                                 parse_dates=True)
            run_df = run_df.drop(columns=['timestamp', 'metric'])
            run_df = run_df[run_df['resource'] == 'blackbox-exporter']
            run_df['value'] = run_df['value'].astype(float)
            run_df['replicas'] = s
            run_df['run'] = r
            df = pd.concat([df, run_df])
    mean = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).mean()
    std = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).sem()

    print("Shared-T*P1 CPU")
    print(mean)

    plt.errorbar(df['replicas'].unique(), mean, yerr=std, capsize=4)
    plt.title('Shared-T*P1 Probe Holder CPU Consumption', pad=20)
    plt.xlabel('Cartservice Replicas')
    plt.ylabel('CPU (nanocores)')
    plt.xticks(SCALE)
    plt.tight_layout()
    plt.savefig('plots/cpu_shared-t*p1.pdf')
    plt.clf()
    
    # Network In
    df = pd.DataFrame(columns=['replicas', 'run', 'resource', 'value'])
    for s in SCALE:
        for r in RUNS:
            csv_filepath = f"{DATA_DIR}/cartservice/{s}/{r}/network_in-probe-holders.csv"
            run_df = pd.read_csv(csv_filepath, names=['timestamp', 'resource', 'metric', 'value'], header=None,
                                 parse_dates=True)
            run_df = run_df[run_df['resource'] == 'blackbox-exporter']
            run_df['value'] = run_df['value'].astype(float)
            run_df['replicas'] = s
            run_df['run'] = r
            df = pd.concat([df, run_df])
    df = _resample_cumulative_network_data(df, 'network_in')
    df = df.drop(columns=['timestamp', 'metric'])
    
    mean = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).mean()
    std = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).sem()
    
    print("Shared-T*P1 Network In")
    print(mean)

    plt.errorbar(df['replicas'].unique(), mean, yerr=std, capsize=4)
    plt.title('Shared-T*P1 Probe Holder Network In', pad=20)
    plt.xlabel('Cartservice Replicas')
    plt.ylabel('Network In (MiB)')
    plt.xticks(SCALE)
    plt.tight_layout()
    plt.savefig('plots/network_in_shared-t*p1.pdf')
    plt.clf()

    # Network Out
    df = pd.DataFrame(columns=['replicas', 'run', 'resource', 'value'])
    for s in SCALE:
        for r in RUNS:
            csv_filepath = f"{DATA_DIR}/cartservice/{s}/{r}/network_out-probe-holders.csv"
            run_df = pd.read_csv(csv_filepath, names=['timestamp', 'resource', 'metric', 'value'], header=None,
                                 parse_dates=True)
            run_df = run_df[run_df['resource'] == 'blackbox-exporter']
            run_df['value'] = run_df['value'].astype(float)
            run_df['replicas'] = s
            run_df['run'] = r
            df = pd.concat([df, run_df])
    df = _resample_cumulative_network_data(df, 'network_out')
    df = df.drop(columns=['timestamp', 'metric'])

    mean = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).mean()
    std = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).sem()
    
    print("Shared-T*P1 Network Out")
    print(mean)

    plt.errorbar(df['replicas'].unique(), mean, yerr=std, capsize=4)
    plt.title('Shared-T*P1 Probe Holder Network Out', pad=20)
    plt.xlabel('Cartservice Replicas')
    plt.ylabel('Network Out (MiB)')
    plt.xticks(SCALE)
    plt.tight_layout()
    plt.savefig('plots/network_out_shared-t*p1.pdf')
    plt.clf()


def _generate_reserved_probe_holder_plots():
    # Memory
    df = pd.DataFrame(columns=['replicas', 'run', 'resource', 'value'])
    for s in SCALE:
        for r in RUNS:
            csv_filepath = f"{DATA_DIR}/redis-cart/{s}/{r}/memory-probe-holders.csv"
            run_df = pd.read_csv(csv_filepath, names=['timestamp', 'resource', 'metric', 'value'], header=None,
                                 parse_dates=True)
            run_df = run_df.drop(columns=['timestamp', 'metric'])
            run_df = run_df[run_df['resource'].str.contains('redis-exporter-')]
            run_df['value'] = run_df['value'] / 1024 / 1024 / 1024  # Bytes to GiB
            run_df['value'] = run_df['value'].astype(float)
            run_df['replicas'] = s
            run_df['run'] = r
            df = pd.concat([df, run_df])
    mean = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).mean()
    std = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).sem()

    print("Reserved-T*P1 Memory")
    print(mean)

    plt.errorbar(df['replicas'].unique(), mean, yerr=std, capsize=4)
    plt.title('Reserved-T*P1 Probe Holders Memory Consumption', pad=20)
    plt.xlabel('Redis-cart Replicas')
    plt.ylabel('Memory (GiB)')
    plt.xticks(SCALE)
    plt.yticks(np.arange(0, 0.3, step=0.1))
    plt.tight_layout()
    plt.savefig('plots/memory_reserved-t*p1.pdf')
    plt.clf()

    # CPU
    df = pd.DataFrame(columns=['replicas', 'run', 'resource', 'value'])
    for s in SCALE:
        for r in RUNS:
            csv_filepath = f"{DATA_DIR}/redis-cart/{s}/{r}/cpu-probe-holders.csv"
            run_df = pd.read_csv(csv_filepath, names=['timestamp', 'resource', 'metric', 'value'], header=None,
                                 parse_dates=True)
            run_df = run_df.drop(columns=['timestamp', 'metric'])
            run_df = run_df[run_df['resource'].str.contains('redis-exporter-')]
            run_df['value'] = run_df['value'].astype(float)
            run_df['replicas'] = s
            run_df['run'] = r
            df = pd.concat([df, run_df])
    mean = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).mean()
    std = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).sem()

    print("Reserved-T*P1 CPU")
    print(mean)

    plt.errorbar(df['replicas'].unique(), mean, yerr=std, capsize=4)
    plt.title('Reserved-T*P1 Probe Holders CPU Consumption', pad=20)
    plt.xlabel('Redis-cart Replicas')
    plt.ylabel('CPU (nanocores)')
    plt.xticks(SCALE)
    plt.tight_layout()
    plt.savefig('plots/cpu_reserved-t*p1.pdf')
    plt.clf()

    # Network In
    df = pd.DataFrame(columns=['replicas', 'run', 'resource', 'value'])
    for s in SCALE:
        for r in RUNS:
            csv_filepath = f"{DATA_DIR}/redis-cart/{s}/{r}/network_in-probe-holders.csv"
            run_df = pd.read_csv(csv_filepath, names=['timestamp', 'resource', 'metric', 'value'], header=None,
                                 parse_dates=True)
            run_df = run_df[run_df['resource'].str.contains('redis-exporter-')]
            run_df['value'] = run_df['value'].astype(float)
            run_df['replicas'] = s
            run_df['run'] = r
            df = pd.concat([df, run_df])
    df = _resample_cumulative_network_data(df, 'network_in')
    df = df.drop(columns=['timestamp', 'metric'])

    mean = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).mean()
    std = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).sem()

    print("Reserved-T*P1 Network In")
    print(mean)

    plt.errorbar(df['replicas'].unique(), mean, yerr=std, capsize=4)
    plt.title('Reserved-T*P1 Probe Holders Network In', pad=20)
    plt.xlabel('Redis-cart Replicas')
    plt.ylabel('Network In (MiB)')
    plt.xticks(SCALE)
    plt.tight_layout()
    plt.savefig('plots/network_in_reserved-t*p1.pdf')
    plt.clf()

    # Network Out
    df = pd.DataFrame(columns=['replicas', 'run', 'resource', 'value'])
    for s in SCALE:
        for r in RUNS:
            csv_filepath = f"{DATA_DIR}/redis-cart/{s}/{r}/network_out-probe-holders.csv"
            run_df = pd.read_csv(csv_filepath, names=['timestamp', 'resource', 'metric', 'value'], header=None,
                                 parse_dates=True)
            run_df = run_df[run_df['resource'].str.contains('redis-exporter-')]
            run_df['value'] = run_df['value'].astype(float)
            run_df['replicas'] = s
            run_df['run'] = r
            df = pd.concat([df, run_df])
    df = _resample_cumulative_network_data(df, 'network_out')
    df = df.drop(columns=['timestamp', 'metric'])
    
    mean = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).mean()
    std = df.groupby(by=['replicas', 'resource', 'run'])['value'].median().groupby(
        by=['replicas', 'run']).sum().groupby(
        by=['replicas']).sem()

    print("Reserved-T*P1 Network Out")
    print(mean)

    plt.errorbar(df['replicas'].unique(), mean, yerr=std, capsize=4)
    plt.title('Reserved-T*P1 Probe Holders Network Out', pad=20)
    plt.xlabel('Redis-cart Replicas')
    plt.ylabel('Network Out (MiB)')
    plt.xticks(SCALE)
    plt.tight_layout()
    plt.savefig('plots/network_out_reserved-t*p1.pdf')
    plt.clf()


def main():
    os.makedirs('plots', exist_ok=True)
    _generate_shared_probe_holder_plots()
    _generate_reserved_probe_holder_plots()


if __name__ == '__main__':
    main()
