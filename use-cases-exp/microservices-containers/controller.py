import os
import subprocess
import time

METRICS = ['cpu', 'memory', 'network_in', 'network_out']
RESOURCES = ['targets', 'probe-holders']
SCALE = [1, 2, 4, 8, 16]


def _run_logstash():
    result = subprocess.run(["kubectl", "apply", "-f", "logstash.yaml"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _run_apps():
    result = subprocess.run(["kubectl", "apply", "-k", "."], capture_output=True, text=True, check=True)
    print(result.stdout)


def _run_metricbeat():
    result = subprocess.run(["kubectl", "apply", "-f", "metricbeat.yaml"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _scale_deployment(deployment: str, replicas: int):
    result = subprocess.run(["kubectl", "scale", "deployment", f"{deployment}", "--replicas", f"{replicas}"],
                            capture_output=True, text=True, check=True)
    print(result.stdout)


def _destroy_apps():
    result = subprocess.run(["kubectl", "delete", "-k", "."], capture_output=True, text=True, check=True)
    print(result.stdout)


def _destroy_metricbeat():
    result = subprocess.run(["kubectl", "delete", "-f", "metricbeat.yaml"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _destroy_logstash():
    result = subprocess.run(["kubectl", "delete", "-f", "logstash.yaml"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _get_csv_results(data_dir: str):
    os.makedirs(data_dir, exist_ok=True)

    result = subprocess.run(["kubectl get pods -o=name | grep logstash | sed 's/^.\{4\}//'"], capture_output=True,
                            text=True, check=True, shell=True)
    logstash_pod = result.stdout.strip()

    for r in RESOURCES:
        for m in METRICS:
            result = subprocess.run(
                ["kubectl", "cp", f"{logstash_pod}:/usr/share/logstash/{m}-{r}.csv", f"{data_dir}/{m}-{r}.csv"],
                capture_output=True, text=True, check=True)
            print(result.stdout)


def _run_experiment(service_to_scale: str):
    for replicas in SCALE:
        for run in range(1, 4):
            _run_apps()
            _run_logstash()
            _scale_deployment(service_to_scale, replicas)
            time.sleep(60)
            _run_metricbeat()
            time.sleep(30)
            time.sleep(60 * 10)
            _destroy_metricbeat()
            _destroy_apps()
            _get_csv_results(f"data/{service_to_scale}/{replicas}/{run}")
            _destroy_logstash()


def main():
    _run_experiment('redis-cart')
    _run_experiment('cartservice')


if __name__ == "__main__":
    main()
