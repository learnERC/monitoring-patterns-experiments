import json
import os
import sys
import time

import ansible_runner

SERVICES = ['adservice', 'cartservice', 'checkoutservice', 'currencyservice', 'emailservice', 'frontend',
            'paymentservice', 'productcatalogservice', 'recommendationservice', 'redis', 'shippingservice']
SCALE = [1, 2, 4, 8, 16]

RESOURCE_GROUP = 'mariani-vm-rg'


def _create_vm(name: str, vm_size: str):
    result = ansible_runner.run(private_data_dir='.', playbook='create.yml', roles_path='./roles', extravars={
        'name': name, 'vm_size': vm_size,
    })
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _deploy_stack():
    result = ansible_runner.run(private_data_dir='.', playbook='deploy.yml', roles_path='./roles', extravars={
        'install_logstash': True
    }, inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _destroy_vm(name):
    result = ansible_runner.run(private_data_dir='.', playbook='teardown.yml', roles_path='./roles',
                                inventory='inventory.azure_rm.yml',
                                extravars={'resource_group': RESOURCE_GROUP, 'name': name})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _teardown(service_to_scale: str):
    _destroy_vm('elasticsearch')
    _destroy_vm('probe-holder')
    for service in SERVICES:
        if service == service_to_scale:
            for r in range(1, SCALE[len(SCALE) - 1] + 1):
                name = f"{service_to_scale}-{r}"
                _destroy_vm(name)
        else:
            _destroy_vm(service)


def _start_logstash():
    result = ansible_runner.run(private_data_dir='.', playbook='start_logstash.yml', roles_path='./roles', extravars={
        'start_logstash': True, 'install_logstash': False
    }, inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _stop_logstash():
    result = ansible_runner.run(private_data_dir='.', playbook='stop_logstash.yml', roles_path='./roles', extravars={
        'stop_logstash': True, 'install_logstash': False
    }, inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _stop_metricbeat_exp():
    result = ansible_runner.run(private_data_dir='.', playbook='stop_metricbeat_exp.yml', roles_path='./roles',
                                inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _start_metricbeat_exp():
    result = ansible_runner.run(private_data_dir='.', playbook='start_metricbeat_exp.yml', roles_path='./roles',
                                inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _get_csv_results(data_dir: str):
    os.makedirs(data_dir, exist_ok=True)
    result = ansible_runner.run(private_data_dir='.', playbook='get_results.yml', roles_path='./roles', extravars={
        'data_dir': data_dir
    }, inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _reboot_and_wait(name: str):
    result = ansible_runner.run(private_data_dir='.', playbook='reboot_and_wait.yml', roles_path='./roles',
                                inventory='inventory.azure_rm.yml', extravars={'resource_group': RESOURCE_GROUP, 'name': name})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _reboot_all(service_to_scale: str, replica_number: int):
    for service in SERVICES:
        if service == service_to_scale:
            for r in range(1, replica_number + 1):
                name = f"{service_to_scale}-{r}"
                _reboot_and_wait(name)
        else:
            _reboot_and_wait(service)
    _reboot_and_wait('probe-holder')


def _execute_exp(service_to_scale: str):
    _create_vm('elasticsearch', 'Standard_B2ms')
    _create_vm('probe-holder', 'Standard_B1s')
    exp_timestamps = {}
    for replica_number in SCALE:
        exp_timestamps[replica_number] = []
        for service in SERVICES:
            if service == service_to_scale:
                for r in range(1, replica_number + 1):
                    name = f"{service_to_scale}-{r}"
                    _create_vm(name, 'Standard_B1s')
            else:
                _create_vm(service, 'Standard_B1s')
        _deploy_stack()
        time.sleep(60)
        for run in range(1, 4):
            _start_logstash()
            _start_metricbeat_exp()
            exp_timestamps[replica_number].append({'start': time.time_ns(), 'end': None})
            time.sleep(60 * 10)
            exp_timestamps[replica_number][run - 1]['end'] = time.time_ns()
            _stop_metricbeat_exp()
            _stop_logstash()
            _get_csv_results(f"data/{service_to_scale}/{replica_number}/{run}")
            # _reboot_all(service_to_scale, replica_number)
    with open(f"data/{service_to_scale}/exp_timestamps.json", mode='w') as f:
        json.dump(exp_timestamps, f)
    _teardown(service_to_scale)


def main():
    _execute_exp('recommendationservice')
    _execute_exp('paymentservice')


if __name__ == "__main__":
    main()
