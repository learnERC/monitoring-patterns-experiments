import time

from controller import _create_vm, _destroy_vm, _deploy_stack, _start_logstash, _start_metricbeat_exp, \
    _stop_metricbeat_exp, _stop_logstash, _get_csv_results


def _teardown(baseline_target: str):
    _destroy_vm('elasticsearch')
    _destroy_vm(baseline_target)


def _execute_exp(baseline_target: str):
    _create_vm('elasticsearch', 'Standard_B2ms')
    _create_vm(baseline_target, 'Standard_B1s')
    
    _deploy_stack()
    time.sleep(60)
    for run in range(1, 4):
        _start_logstash()
        _start_metricbeat_exp()
        time.sleep(60 * 10)
        _stop_metricbeat_exp()
        _stop_logstash()
        _get_csv_results(f"data/baseline/{run}")

    _teardown(baseline_target)


def main():
    _execute_exp('recommendationservice')


if __name__ == "__main__":
    main()
