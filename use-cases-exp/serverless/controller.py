import argparse
import os
import subprocess
import time

METRICS = ['cpu', 'memory', 'network_in', 'network_out']
RESOURCES = ['targets', 'probe-holders']
SCALE = [1, 2, 4, 8, 16]
DEPLOYMENT = 'carts-get'


def _parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    return parser.parse_args()


def _run_logstash():
    result = subprocess.run(["kubectl", "apply", "-f", "logstash.yaml"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _run_apps():
    result = subprocess.run(["kubectl", "apply", "--recursive", "-f", "sock-shop-openfaas"], capture_output=True, text=True, check=True)
    print(result.stdout)
    
    result = subprocess.run(["kubectl", "apply", "-k", "cadvisor"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _patch_prometheus():
    result = subprocess.run(["kubectl", "apply", "-f", "prometheus-config.yaml"], capture_output=True, text=True, check=True)
    print(result.stdout)
    result = subprocess.run(["kubectl get pods -n openfaas -o=name | grep prometheus | sed 's/^.\{4\}//'"], capture_output=True,
                            text=True, check=True, shell=True)
    prometheus_pod = result.stdout.strip()
    result = subprocess.run(["kubectl", "delete", "pods", "-n", "openfaas", f"{prometheus_pod}"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _run_metricbeat():
    result = subprocess.run(["kubectl", "apply", "-f", "metricbeat.yaml"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _scale_deployment(deployment: str, replicas: int):
    result = subprocess.run(["kubectl", "scale", "deployment", "-n", "openfaas-fn", f"{deployment}", "--replicas", f"{replicas}"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _destroy_apps():
    result = subprocess.run(["kubectl", "delete", "--recursive", "-f", "sock-shop-openfaas"], capture_output=True, text=True, check=True)
    print(result.stdout)
    
    result = subprocess.run(["kubectl", "delete", "-k", "cadvisor"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _destroy_metricbeat():
    result = subprocess.run(["kubectl", "delete", "-f", "metricbeat.yaml"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _destroy_logstash():
    result = subprocess.run(["kubectl", "delete", "-f", "logstash.yaml"], capture_output=True, text=True, check=True)
    print(result.stdout)


def _get_csv_results(data_dir: str):
    os.makedirs(data_dir, exist_ok=True)

    result = subprocess.run(["kubectl get pods -o=name | grep logstash | sed 's/^.\{4\}//'"], capture_output=True,
                            text=True, check=True, shell=True)
    logstash_pod = result.stdout.strip()

    for r in RESOURCES:
        for m in METRICS:
            result = subprocess.run(
                ["kubectl", "cp", f"{logstash_pod}:/usr/share/logstash/{m}-{r}.csv", f"{data_dir}/{m}-{r}.csv"],
                capture_output=True, text=True, check=True)
            print(result.stdout)


def main(args):
    for replicas in SCALE:
        for run in range(1, 4):
            _run_apps()
            _patch_prometheus()
            _run_logstash()
            _scale_deployment(DEPLOYMENT, replicas)
            time.sleep(60)
            _run_metricbeat()
            time.sleep(30)
            time.sleep(60 * 10)
            _destroy_metricbeat()
            _destroy_apps()
            _get_csv_results(f"data/{DEPLOYMENT}/{replicas}/{run}")
            _destroy_logstash()


if __name__ == "__main__":
    main(_parse_args())
