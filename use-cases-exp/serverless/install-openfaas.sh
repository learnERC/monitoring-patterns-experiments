#!/bin/sh

kubectl apply -f https://raw.githubusercontent.com/openfaas/faas-netes/master/namespaces.yml
helm install openfaas openfaas/openfaas \
    --version 11.1.0 \
    --namespace openfaas \
    --set operator.create=true \
    --set generateBasicAuth=true \
    --set basic_auth=true \
    --set functionNamespace=openfaas-fn \
    --set serviceType=LoadBalancer

PASSWORD=$(kubectl -n openfaas get secret basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode)
OPENFAAS_EXTERNAL_IP=$(kubectl -n openfaas get services gateway-external -o jsonpath="{.status.loadBalancer.ingress[0].ip}")
OPENFAAS_URL="http://$OPENFAAS_EXTERNAL_IP:8080"

faas-cli login -g "$OPENFAAS_URL" -u admin -p "$PASSWORD"
