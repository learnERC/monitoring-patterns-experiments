import sys

import ansible_runner


def main():
    result = ansible_runner.run(private_data_dir='.', playbook='clear-container-cluster.yml', roles_path='./roles',
                                extravars={'context': 'lta-kubernetes-cluster'})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


if __name__ == "__main__":
    main()
