# Monitoring Probe Deployment Patterns for Cloud-Native Applications: Definition and Empirical Assessment

This is repository contains the experimental material of the paper Alessandro Tundo, Marco Mobilio,  Oliviero Riganelli and Leonardo Mariani. "Monitoring Probe Deployment Patterns for Cloud-Native Applications: Definition and Empirical Assessment". Submitted to IEEE Transactions of Services Computing (September 2022).

## Requirements
Experiments use Microsoft Azure Compute instances to run VM-based experiments, a valid subscription is required.
Container-based experiments can run on any Kubernetes installation. At the time of writing, we used Kubernetes v1.20.9 for patterns experimentscl.

We employ Ansible and Ansible Runner to orchestrate the execution of the experiments.
All the software requirements can be installed via using `pip`.
We strongly advise to create a virtual environment to not mess up the dependencies.

```bash
pip install -r requirements.txt
pip install -r requirements-azure.txt
pip install -r requirements-kubernetes.txt
```

Also, we collect data and generate CSV dataset using [Metricbeat](https://www.elastic.co/beats/metricbeat) and [Logstash](https://www.elastic.co/logstash/).
Metricbeat and Logstash are installed by our Ansible roles for container-based experiments, while an accessible Logstash instance for the VM-based experiments must be provided.

The Logstash pipeline configuration to generate CSV files is provided by [logstash-csv-pipeline-vm.conf](logstash-csv-pipeline-vm.conf).
> Logstash [Csv filter plugin](https://www.elastic.co/guide/en/logstash/current/plugins-filters-csv.html) must be installed.

## Repository structure

- [dataset](dataset): raw and the cleaned dataset directories. It also contains the `pattern-runs.json` file to keep track of the experiment runs.
- [plots](plots): generated plots directory
- [roles](roles): the Ansible roles directory
- [experiments.json](experiments.json): contains the experiments plan

## How to run experiments
> Empty the `pattern-runs.json` file to discard previous runs (e.g., `echo '{}' > dataset/pattern-runs.json`)

### VM-based patterns

```bash
# See the experiments.json to discover the required numbers for each pattern
python3 create_resources.py -t NUMBER_OF_TARGETS -m NUMBER_OF_MONITORING_UNITS
python3 controller.py -p PATTERN_NAME -l LOGSTASH_HOST
```

### Container-based patterns

```bash
# See the experiments.json to discover the required numbers for each pattern
python3 setup_container_cluster.py
python3 controller_containers.py -p PATTERN_NAME
```

> CSV files can be found in the Logstash data directory, they should be then inserted in the [dataset/raw](dataset/raw) directory.

## How to generate plots

First, you must split and clean the raw dataset using the provided [split_and_clean_raw_dataset.py](split_and_clean_raw_dataset.py) script:

```bash
python3 split_and_clean_raw_dataset.py
```

Then, you can run the [generate_plots.py](generate_plots.py) scripts to obtain the plots:

```bash
python3 generate_plots.py
```
