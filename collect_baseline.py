import os
import sys
import time

import ansible_runner

USE_CASES_EXP_DIR = f"{os.getcwd()}/use-cases-exp/microservices-vms"


def _create_vm(name: str, vm_size: str, offer: str = '0001-com-ubuntu-server-jammy', sku: str = '22_04-lts-gen2'):
    result = ansible_runner.run(private_data_dir='.', playbook=f'{USE_CASES_EXP_DIR}/create.yml', roles_path=f'{USE_CASES_EXP_DIR}/roles', extravars={
        'name': name, 'vm_size': vm_size, 'offer': offer, 'sku': sku,
    })
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _destroy_vm(name):
    result = ansible_runner.run(private_data_dir='.', playbook=f'{USE_CASES_EXP_DIR}/teardown.yml', roles_path=f'{USE_CASES_EXP_DIR}/roles',
                                inventory='inventory.azure_rm.yml', extravars={'resource_group': 'mariani-vm-rg', 'name': name})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _configure():
    result = ansible_runner.run(private_data_dir='.', playbook='configure_baseline.yml',
                                roles_path='./roles',
                                inventory='inventory.azure_rm.yml',
                                extravars={'install_logstash': True})
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _teardown(baseline_target: str):
    _destroy_vm('elasticsearch')
    _destroy_vm(baseline_target)


def _start_logstash():
    result = ansible_runner.run(private_data_dir='.', playbook=f'{USE_CASES_EXP_DIR}/start_logstash.yml', roles_path=f'{USE_CASES_EXP_DIR}/roles', extravars={
        'start_logstash': True, 'install_logstash': False
    }, inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _stop_logstash():
    result = ansible_runner.run(private_data_dir='.', playbook=f'{USE_CASES_EXP_DIR}/stop_logstash.yml', roles_path=f'{USE_CASES_EXP_DIR}/roles', extravars={
        'stop_logstash': True, 'install_logstash': False
    }, inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _stop_metricbeat_exp():
    result = ansible_runner.run(private_data_dir='.', playbook=f'{USE_CASES_EXP_DIR}/stop_metricbeat_exp.yml', roles_path=f'{USE_CASES_EXP_DIR}/roles', inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _start_metricbeat_exp():
    result = ansible_runner.run(private_data_dir='.', playbook=f'{USE_CASES_EXP_DIR}/start_metricbeat_exp.yml', roles_path=f'{USE_CASES_EXP_DIR}/roles', inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _get_csv_results(data_dir: str):
    os.makedirs(data_dir, exist_ok=True)
    result = ansible_runner.run(private_data_dir='.', playbook=f'{USE_CASES_EXP_DIR}/get_results.yml', roles_path=f'{USE_CASES_EXP_DIR}/roles', extravars={
        'data_dir': data_dir
    }, inventory='inventory.azure_rm.yml')
    print("{}: {}".format(result.status, result.rc))
    print("Final status: {}".format(result.stats))
    if result.rc == 2:
        sys.exit(2)


def _execute_exp(baseline_target: str):
    _create_vm('elasticsearch', 'Standard_B2ms')
    _create_vm(baseline_target, 'Standard_A2_v2', 'UbuntuServer', '18.04-LTS')
    
    _configure()
    
    time.sleep(60)
    for run in range(1, 4):
        _start_logstash()
        _start_metricbeat_exp()
        time.sleep(60 * 10)
        _stop_metricbeat_exp()
        _stop_logstash()
        _get_csv_results(f"{os.getcwd()}/dataset/baseline/{run}")

    _teardown(baseline_target)


def main():
    _execute_exp('target-1')


if __name__ == "__main__":
    main()
