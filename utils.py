import json
from datetime import datetime

EXPERIMENTS_PLAN_FILE = './experiments.json'

PATTERN_RUNS_FILE = './dataset/pattern-runs.json'

METRICS = [
    'cpu',
    'memory',
    'network_in',
    'network_out'
]

VM_PATTERNS = [
    'RESERVED_SIDECAR_MULTI_PROBE',
    'SHARED_SIDECAR_MULTI_PROBE',
    'RESERVED_GLOBAL_UNIT_MULTI_PROBE',
    'SHARED_GLOBAL_UNIT_MULTI_PROBE',
    'PARTIALLY_SHARED_UNIT_STAR',
    'PARTIALLY_SHARED_SIDECAR_STAR',
    'INTERNAL_UNIT_STAR',
]

CONTAINER_PATTERNS = [
    'FULLY_SHARED_SIDECAR_1',
    'RESERVED_SIDECAR_1',
    'RESERVED_UNIT_1',
    'FULLY_SHARED_UNIT_1',
]

EXPERIMENT_IDS = [
    'INCREASING_KPIS_1',
    'INCREASING_KPIS_2',
    'INCREASING_TARGETS_1',
    'INCREASING_TARGETS_2',
    'INCREASING_USERS_1',
    'INCREASING_USERS_2',
]

NEW_PATTERN_NAMES = {
    'RESERVED_SIDECAR_MULTI_PROBE': 'Reserved-T1P*',
    'SHARED_SIDECAR_MULTI_PROBE': 'Shared-T1P*',
    'RESERVED_GLOBAL_UNIT_MULTI_PROBE': 'Reserved-T*P*',
    'SHARED_GLOBAL_UNIT_MULTI_PROBE': 'Shared-T*P*',
    'PARTIALLY_SHARED_UNIT_STAR': 'Partially-shared-T*P*',
    'PARTIALLY_SHARED_SIDECAR_STAR': 'Partially-shared-T1P*',
    'INTERNAL_UNIT_STAR': 'Internal-T1P*',
    'FULLY_SHARED_SIDECAR_1': 'Shared-T1P1',
    'RESERVED_SIDECAR_1': 'Reserved-T1P1',
    'RESERVED_UNIT_1': 'Reserved-T*P1',
    'FULLY_SHARED_UNIT_1': 'Shared-T*P1',
}

NEW_RESOURCE_NAMES = {
    'monitoring-units': 'probe holders',
    'targets': 'targets',
}


def load_pattern_experiments(pattern):
    with open(EXPERIMENTS_PLAN_FILE, 'r') as f:
        experiments = json.load(f)
    pattern_experiments = experiments.get(pattern)
    return pattern_experiments


def set_pattern_config_run_start(pattern_config_key, run_id, start_time):
    with open(PATTERN_RUNS_FILE, 'r') as f:
        data = json.load(f)

    timings = {
        'start_time': datetime.timestamp(start_time),
        'end_time': ''
    }
    if data.get(pattern_config_key):
        data[pattern_config_key][str(run_id)] = timings
    else:
        data[pattern_config_key] = {
            str(run_id): timings
        }

    with open(PATTERN_RUNS_FILE, 'w') as f:
        json.dump(data, f)


def set_pattern_config_run_end(pattern_config_key, run_id, end_time):
    with open(PATTERN_RUNS_FILE, 'r') as f:
        data = json.load(f)

    print(data[pattern_config_key][str(run_id)])
    data[pattern_config_key][str(run_id)]['end_time'] = datetime.timestamp(end_time)

    with open(PATTERN_RUNS_FILE, 'w') as f:
        json.dump(data, f)


def is_pattern_config_already_executed(pattern_config_key):
    with open(PATTERN_RUNS_FILE, 'r') as f:
        data = json.load(f)

    if pattern_config_key in data:
        return True
    return False
